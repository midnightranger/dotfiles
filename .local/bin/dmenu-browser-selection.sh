#!/bin/bash

# This is a script file that utilizes dmenu prompt in order to choose
# which browser you widh to run.

# Usually installed browsers:
# firefox
# tor-browser
# surf

# Declarations
BROWSERS_LIST=$HOME/.config/list-browsers.txt
DELIMETER=' '

pgrep -x dmenu && exit

choises=$(cat $BROWSERS_LIST | awk '{print $1}')
[[ "$choises" = "" ]] && exit 1


chosen=$(echo "$choises" | dmenu -i -p "Which browser to open?" | awk '{print $1}')
[[ "$chosen" = "" ]] && exit 1

param1=$(cat $BROWSERS_LIST | grep $chosen | awk '{print $1}')
param2=$(cat $BROWSERS_LIST | grep $chosen | awk '{print $2}')

$param1 $param2
