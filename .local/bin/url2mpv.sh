#!/bin/bash

terminal='/usr/local/bin/st'
urlfromclipboard=$(xsel -ob)
audioplayer='/usr/bin/mpv --no-video'
videoplayer='/usr/bin/mpv --player-operation-mode=pseudo-gui'
selection=$(echo -e "Video\nAudio" | dmenu -i -p "Select format: ")

case $selection in
	Audio) setsid nohup $terminal -e $audioplayer $urlfromclipboard 2>/dev/null;;
	Video) $videoplayer $urlfromclipboard;;
esac
