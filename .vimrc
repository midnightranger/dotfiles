""""""""""""""""""""""""""""""""""""
" Basics
""""""""""""""""""""""""""""""""""""
" Compatible to Vim only
set nocompatible

" enable filetype plugins
filetype plugin on

" enable filetype indent
filetype indent on

" Default encoding
set encoding=utf-8

" Enable syntax highlighting
syntax enable

" show the mathing brackets
set showmatch

" highlight current line
set cursorline

" wrap line
set wrap

" enable spell-checking
set nospell

" Show cursor position in status bar
set ruler

" To search for available colorschemes, type :colorscheme [space] Ctrl-d
colorscheme elflord

" Set window title
"set titlestring=%t
set title
set laststatus=2

""""""""""""""""""""""""""""""""""""
" Line
""""""""""""""""""""""""""""""""""""
" show line numbers
set number

"""""""""""""""""""""""""""""""""""""
" Indents
"""""""""""""""""""""""""""""""""""""
" replace tabs with spaces
set expandtab
" 1 tab = 2 spaces
set tabstop=2 shiftwidth=2

"""""""""""""""""""""""""""""""""""""
" Search
"""""""""""""""""""""""""""""""""""""
" Ignore case when searching
set ignorecase
set smartcase

" highlight search results (after pressing Enter)
set hlsearch

" highlight all pattern matches WHILE typing the pattern
set incsearch
highlight colorcolumn ctermbg=240

" Automatic reloading of .vimrc
autocmd! bufwritepost .vimrc source %

" Copy selected text to system clipboard (gvim is required)
vnoremap <C-c> "+y
map <C-p> "+P
