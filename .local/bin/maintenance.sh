#!/bin/bash

selection_menu(){ \
    clear
    echo "            Maintenace Menu            "
    echo "***************************************"
    echo
    echo " 1. Check systemd failed services"
    echo " 2. Check log files "
    echo " 3. Perform System Update"
    echo " 4. Clean the Cache Directory"
    echo " 5. Clean the journal"
    echo
    echo "Select Task (1-11, q to exit): "
}

systemd_failed(){ \
    clear
    echo '*************************************'
    echo '*   Check systemd failed services   *'
    echo '*************************************'
    echo
    echo
    systemctl --failed
    echo
    echo "Press ENTER to return to main menu"
}

check_log_files(){ \
    clear
    echo '*************************************'
    echo '*          Check log files          *'
    echo '*************************************'
    echo
    echo
    sudo journalctl -p 3 -xb
    echo
    echo "Press ENTER to return to main menu"
}

system_update(){ \
    clear
    echo '*************************************'
    echo '*       Perform System Update       *'
    echo '*************************************'
    echo
    echo
    update.sh
    echo
    echo "Press ENTER to return to main menu"
}

clear_cache_dir(){ \
    clear
    echo '*************************************'
    echo '*         Clear .cache/ dir         *'
    echo '*************************************'
    echo
    echo
    rm -rf .cache/*
    echo
    echo "Press ENTER to return to main menu"
}

clean_journal(){ \
    clear
    echo '*************************************'
    echo '*         Clear log journal         *'
    echo '*************************************'
    echo
    echo
    sudo journalctl --vacuum-time=2weeks
    echo
    echo "Press ENTER to return to main menu"
}

selection_menu

while true; do

    read choise

        case $choise in
            1) systemd_failed ;;
            2) check_log_files;;
            3) system_update;;
            4) clear_cache_dir;;
            5) clean_journal;;
            q) clear && exit;;
	    "") selection_menu;;
            *) echo "Wrong Selection" && sleep 2 && selection_menu;;
        esac
done
