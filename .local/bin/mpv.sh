#!/bin/bash

terminal='/usr/local/bin/st'
audioplayer='/usr/bin/mpv --no-video'
videoplayer='/usr/bin/mpv --player-operation-mode=pseudo-gui'
selection=$(echo -e "Video\nAudio" | dmenu -i -p "Select format: ")

case $selection in
	Audio) setsid nohup $terminal -e $audioplayer "$1";;
	Video) $videoplayer "$1";;
esac
