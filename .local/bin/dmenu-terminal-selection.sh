#!/bin/bash

# This is a script file that utilizes dmenu prompt in order to choose
# which browser you widh to run.

# Usually installed browsers:
# firefox
# tor-browser
# surf

# Paths declarations
TERMINALS_LIST=$HOME/.config/list-terminals.txt
terminal='/usr/bin/gnome-terminal'

pgrep -x dmenu && exit

choises=$(cat $TERMINALS_LIST | awk '{print $1}')
[[ "$choises" = "" ]] && exit 1


chosen=$(echo "$choises" | dmenu -i -p "Which terminal to open?" | awk '{print $1}')
[[ "$chosen" = "" ]] && exit 1
[[ "$chosen" = "tmux" ]] && $terminal -e tmux

$($chosen)
