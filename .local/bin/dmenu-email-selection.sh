#!/bin/bash

# This is a script file that utilizes dmenu prompt in order to choose
# which browser you widh to run.

# Usually installed browsers:
# firefox
# tor-browser
# surf

# Paths declarations
MAIL_CLIENTS_LIST=$HOME/.config/list-emails.txt

pgrep -x dmenu && exit

choises=$(cat $MAIL_CLIENTS_LIST | awk '{print $1}')
[[ "$choises" = "" ]] && exit 1


chosen=$(echo "$choises" | dmenu -i -p "Which mail-client to open?" | awk '{print $1}')
[[ "$chosen" = "" ]] && exit 1

if [[ "$chosen" = "neomutt" ]]; then
	$TERMINAL -e $chosen
else
	$($chosen)
fi
