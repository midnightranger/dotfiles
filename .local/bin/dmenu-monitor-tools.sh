#!/bin/bash

# This is a script file that utilizes dmenu prompt in order to choose
# which browser you widh to run.

# Usually installed browsers:
# firefox
# tor-browser
# surf

# Paths declarations
MONITOR_TOOLS_LIST=$HOME/.config/list-monitor-tools.txt

pgrep -x dmenu && exit

choises=$(cat $MONITOR_TOOLS_LIST | awk '{print $1}')
[[ "$choises" = "" ]] && exit 1


chosen=$(echo "$choises" | dmenu -i -p "Choose a monitoring tool:" | awk '{print $1}')
[[ "$chosen" = "" ]] && exit 1

$TERMINAL -e $chosen
