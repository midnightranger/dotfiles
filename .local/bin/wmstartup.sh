#!/bin/bash

udiskie &
xset s 0 0 &
xset dpms 0 0 0 &
#/usr/bin/xcalib -d :0 ~/.color/icc/x200.icc &
nitrogen --restore &
#picom &
dunst &
unclutter &
redshift &
#mpd &
nextcloud &
nm-applet &
volumeicon &
xrdb -merge $HOME/.Xresources &
/usr/lib/policykit-1-gnome/polkit-gnome-authentication-agent-1 &
