#!/bin/bash

# This is a script file that utilizes dmenu prompt in order to choose
# which config file you wish to edit.

# Usually editied config files:
# ranger config file ---> /$HOME/.config/ranger/rc.conf
# ranger shortcuts file ---> /$HOME/.config/ranger/shortcuts
# sxhkd config file ---> /$HOME/.config/sxhkd/sxhkdrc

# Paths declarations
CONFIGS_LIST_FILE=$HOME/.config/list-config-files.txt

pgrep -x dmenu && exit

editable=$(cat $CONFIGS_LIST_FILE | awk '{print $1}')
[[ "$editable" = "" ]] && exit 1


chosen=$(echo "$editable" | dmenu -i -p "Which file to edit?" | awk '{print $1}')
[[ "$chosen" = "" ]] && exit 1

$TERMINAL -e vim $(cat $CONFIGS_LIST_FILE | grep $chosen | awk '{print $2}')
