#!/bin/bash

layout=$(xset q|grep -i "LED mask" | awk '{print $10}')	

if [[ $layout = '00000000' ]]; then
	echo '  En '
elif [[ $layout = '00000001' ]]; then 
	echo '  En: Caps ON '
elif [[ $layout = '00001000' ]]; then
	echo '  Gr '
elif [[ $layout = '00001001' ]]; then 
	echo '  Gr: Caps ON '
fi
