#!/bin/bash

# awesome icon declarations
icon_keyboard='\uf11c'
icon_hdd='\uf0a0'
icon_home='\uf015'
icon_volume='\uf028'
icon_mem='\uf2db'
icon_net='\uf0e8'
icon_temp='\uf2c9'
icon_bat_dis='\uf241'
icon_bat_cha='\uf0e7'
icon_bat_recy='\uf1b8'
icon_bat_plug='\uf1e6'
icon_calendar='\uf073'
icon_tray_separator='\uf0d9'

pgrep -x dwmstatus.sh && kill

kb(){ \
	layout=$(xset q|grep -i "LED mask" | awk '{print $10}')	
        
	if [[ $layout = '00000000' ]]; then
		printf $icon_keyboard && echo ' En'
	elif [[ $layout = '00000001' ]]; then 
		printf $icon_keyboard && echo ' En: Caps ON'
	elif [[ $layout = '00001000' ]]; then
		printf $icon_keyboard && echo ' Gr'
	elif [[ $layout = '00001001' ]]; then 
		printf $icon_keyboard && echo ' Gr: Caps ON'
	fi

}
root(){ \
	root_part="/dev/mapper/vgmint-root"
	printf $icon_hdd" " && echo $(df -h | grep $root_part |awk {'print $4'})
}
home(){ \
	home_part=/dev/mapper/volgroup0-lv_home
	printf $icon_home && echo $(df -h | grep $home_part | awk '{ print $4}')
}
audio(){
	printf $icon_volume" " && echo $(awk -F"[][]" '/Left:/ { print $2 }' <(amixer -D pulse sget Master))
}
mem(){ \
	printf $icon_mem" " && echo $(free -h | awk '/^Mem:/ {print $3 "/" $2}')
}
pip(){ \
	printf $icon_net" " && echo $(curl http://ifconfig.co/ip)
}
temp(){ \
	printf $icon_temp" " && echo $(sensors | grep -i 'Core 0:' | awk '{print $3}')
}
bat(){ \
	#printf $icon_bat_dis && echo $(cat /sys/class/power_supply/BAT0/capacity)%
	for battery in /sys/class/power_supply/BAT?
	do
		# Get its remaining capacity and charge status.
		capacity=$(cat "$battery"/capacity 2>/dev/null) || break
		status=$(sed "s/[Dd]ischarging/$(printf $icon_bat_dis)/;s/[Nn]ot charging//;s/[Cc]harging/$(printf $icon_bat_cha)/;s/[Uu]nknown/$(printf $icon_bat_recy)/;s/[Ff]ull/$(printf $icon_bat_plug)/" "$battery"/status)
		# If it is discharging and 25% or less, we will add a ❗ as a warning.
		[ "$capacity" -le 25 ] && [ "$status" = "🔋" ] && warn="❗"

		printf "%s%s%s%% " "$status" "$warn" " $capacity"
		unset warn
	done | sed 's/ *$//'
}

t(){ \
	printf $icon_calendar" " && echo $(date '+%a, %d %b %Y  %I:%M%p')
}
vpnstatus(){ \
	vpncheck=$(ip addr | cut -d " " -f 2 | grep -i tun0)
	case $vpncheck in
		"") echo 'VPN: down';;
		tun0:) echo 'VPN: up';;
	esac
}	
tor(){ \
	torstatus=$(systemctl is-active tor)
	case $torstatus in
		inactive) echo 'TOR: down';;
		active) echo 'TOR: up';;
	esac
}	
separator(){ \
	printf $icon_tray_separator
}	

while true; do
	xsetroot -name "$(vpnstatus)   [$(bat)]   [$(kb)]   $(t) "
	sleep 1s
done
