#!/usr/bin/bash

# Terminate already running bar instances
killall -q polybar
# If all your bars have ipc enabled, you can also use 
# polybar-msg cmd quit

# Launch bar
polybar bar >>/tmp/polybar2.log 2>&1 &

echo "Bars launched..."
