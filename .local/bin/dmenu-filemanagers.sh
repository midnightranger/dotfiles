#!/bin/bash

# This is a script file that utilizes dmenu prompt in order to choose
# which browser you widh to run.

# Usually installed browsers:
# firefox
# tor-browser
# surf

# Paths declarations
FILE_MANAGERS_LIST=$HOME/.config/list-filemanagers.txt

pgrep -x dmenu && exit

choises=$(cat $FILE_MANAGERS_LIST | awk '{print $1}')
[[ "$choises" = "" ]] && exit 1


chosen=$(echo "$choises" | dmenu -i -p "Which file manager to open?" | awk '{print $1}')
[[ "$chosen" = "" ]] && exit 1

if [[ "$chosen" = "ranger" ]]; then
	$TERMINAL -e $chosen
elif [[ "$chosen" = "mc" ]]; then
	$TERMINAL -e $chosen
else
	$($chosen)
fi
