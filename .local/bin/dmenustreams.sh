#!/bin/bash

killall -q vlc

torstatus=$(systemctl is-active tor)
streamspath='/home/yannis/Cloud/Streams'
player='/usr/bin/vlc --started-from-file'

case $torstatus in
	inactive) $player $(du -a $streamspath | grep .strm.txt$ | cut -f2 | dmenu -i -l 30 | xargs -I "{}" cat {}) 2>/dev/null;;
	active) proxychains -q $player $(du -a $streamspath | grep .strm.txt$ | cut -f2 | dmenu -i -l 30 | xargs -I "{}" cat {}) 2>/dev/null;;
esac
