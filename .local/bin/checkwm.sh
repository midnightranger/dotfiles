#!/bin/bash

wm=$(echo $GDMSESSION)

case $wm in
        bspwm) wmstartup.sh;;
        i3) wmstartup.sh;;
        dwm) wmstartup.sh && dwmstatus.sh;;
esac
