#!/bin/bash

vpncheck=$(ip addr | cut -d " " -f 2 | grep -i tun0)

case $vpncheck in
	"") echo 'VPN: down';;
	tun0:) echo 'VPN: up';;
esac
