#!/bin/bash

# This is a script file that utilizes dmenu prompt in order to choose
# which browser you widh to run.

# Usually installed browsers:
# firefox
# tor-browser
# surf

# Paths declarations
PLAYERS_LIST=$HOME/.config/list-players.txt

pgrep -x dmenu && exit

choises=$(cat $PLAYERS_LIST | awk '{print $1}')
[[ "$choises" = "" ]] && exit 1


chosen=$(echo "$choises" | dmenu -i -p "Which media player to open?" | awk '{print $1}')
[[ "$chosen" = "" ]] && exit 1

if [[ "$chosen" = "mocp" ]]; then
	$TERMINAL -e $chosen
elif [[ "$chosen" = "ncmpcpp" ]]; then
	$TERMINAL -e $chosen
elif [[ "$chosen" = "castero" ]]; then
	$TERMINAL -e $chosen
else
	$($chosen)
fi
