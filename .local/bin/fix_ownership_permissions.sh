#!/bin/bash
# Load up your directories...
Directories[0]="/archives/Archives"
Directories[1]="/archives/EBooks"
Directories[2]="/archives/Music"
Directories[3]="/archives/Photos"
Directories[4]="/archives/Software"
Directories[5]="/archives/Videos"
# add more if needbe

# Load up your restricted directories
Restricted[0]='/archives/Archives/Confidential'

# Load up your special directories
Torrents[0]='/archives/Torrents'

# Setup find correctly.
export IFS=$'\n'

# Loop through our array.
for x in ${Directories[@]}
    do 
        # Find all directories & subdirectories
        for i in $(find $x -type d) 
            do 
                # Fix Permissions
                chmod -c 750 $i
                chown -c yannis:smbusers $i
            done

        # Find all Files
        for i in $(find $x -type f) 
            do 
                # Fix Permissions
                chmod -c 640 $i
                chown -c yannis:smbusers $i
            done
    done

# Reinstated confidentiality in restricted directories
for y in ${Restricted[@]}
    do
        # Find all directories & subdirectories
        for j in $(find $y -type d)
            do
                chmod -c 700 $j
                chown -c yannis:smbusers $j
            done

        # Find all Files
        for j in $(find $y -type f)
            do
                chmod -c 600 $j
                chown -c yannis:smbusers $j
            done
    done

# Reinstated credentials in Torrents directories
for y in ${Torrents[@]}
    do
        # Find all directories & subdirectories
        for j in $(find $y -type d)
            do
                chmod -c 775 $j
                chown -c debian-transmission:debian-transmission $j
            done

        # Find all Files
        for j in $(find $y -type f)
            do
                chmod -c 664 $j
                chown -c debian-transmission:debian-transmission $j
            done
    done
