#   _       _            __ _       _                              
#  (_) ___ | |__  _ __  / _| | ___ | | ____ _ ___  __  ___   _ ____
#  | |/ _ \| '_ \| '_ \| |_| |/ _ \| |/ / _` / __| \ \/ / | | |_  /
#  | | (_) | | | | | | |  _| | (_) |   < (_| \__ \_ >  <| |_| |/ / 
# _/ |\___/|_| |_|_| |_|_| |_|\___/|_|\_\__,_|___(_)_/\_\\__, /___|
#|__/                                                    |___/     
#
# Most of the below entries are pretty much standard in many distros. I just tried to consolidate them to my liking.

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

stty -ixon # Disable ctrl-s (pauses terminal) and ctrl-q (resumes terminal)

# History settings
HISTSIZE=1000
HISTFILESIZE=2000

# Prompt
export PS1="\[$(tput bold)\]\[$(tput setaf 1)\][\[$(tput setaf 3)\]\u\[$(tput setaf 2)\]@\[$(tput setaf 4)\]\h \[$(tput setaf 5)\]\W\[$(tput setaf 1)\]]\[$(tput setaf 7)\]\\$ \[$(tput sgr0)\]"

### SHOPT
shopt -s autocd # Allows you to cd into a Directory with only typing its name
shopt -s cdspell # autocorrects cd misspellings
shopt -s cmdhist # save multi-line commands in history as single line
shopt -s dotglob
shopt -s histappend # do not overwrite history
shopt -s checkwinsize # checks term size when bash regains control

#ignore upper and lowercase when TAB completion
bind "set completion-ignore-case on"

# Help for any command et al
helpme ()
{
	man $1 || apropos $1
}	

### ARCHIVE EXTRACTION
# "Stolen" with pride from DistroTube's gitlab.
# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;      
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# ALIASES - I prefer to keep my aliases in a different file....for housekeeping!!!
[[ -f ~/.bash_aliases ]] && . ~/.bash_aliases
