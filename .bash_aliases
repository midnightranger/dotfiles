#    _       _            __ _       _                              
#   (_) ___ | |__  _ __  / _| | ___ | | ____ _ ___  __  ___   _ ____
#   | |/ _ \| '_ \| '_ \| |_| |/ _ \| |/ / _` / __| \ \/ / | | |_  /
#   | | (_) | | | | | | |  _| | (_) |   < (_| \__ \_ >  <| |_| |/ / 
#  _/ |\___/|_| |_|_| |_|_| |_|\___/|_|\_\__,_|___(_)_/\_\\__, /___|
# |__/                                                    |___/     

## System Maintainance
#  pacman
alias apt='sudo apt'
alias purge='sudo apt purge'
alias U="update.sh"
alias M="maintenance.sh"
#  uchecker
alias uchecker='curl -s -L https://kernelcare.com/uchecker | sudo python'

# get top process eating memory
alias psmem='ps auxf | sort -nr -k 4'
alias psmem10='ps auxf | sort -nr -k 4 | head -10'

# get top process eating cpu ##
alias pscpu='ps auxf | sort -nr -k 3'
alias pscpu10='ps auxf | sort -nr -k 3 | head -10'

# systemd
alias SS='sudo systemctl'
alias sdn='sudo systemctl poweroff'
alias rbt='sudo systemctl reboot'
alias jctl="journalctl -p 3 -xb" # get error messages from journalctl

# gpg encryption
# verify signature for isos
alias gpg-check='gpg2 --keyserver-options auto-key-retrieve --verify'
# receive the key of a developer
alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"

# network firewall et al
alias ufw='sudo ufw'
alias netstat='sudo netstat'
alias iftop='sudo iftop -i wls1'
alias ifconfig='sudo ifconfig'
alias iwconfig='sudo iwconfig'
alias arp='sudo arp'

# SSH
alias ssha='eval $(ssh-agent) && ssh-add $HOME/.ssh/id_ed25519'
alias h='$HOME/.local/bin/dmenu-sshosts-selection.sh'

# git: regular commands
alias status='git status'
alias add='git add'
alias commit='git commit'
alias push='git push -u origin master'
alias pull='git pull'

# filesystems mounting
alias m='sudo mount'
alias u='sudo umount'
alias vault-m='mount_sshfs.sh'
alias vault-u='umount_sshfs.sh'

## Command Aliases
# File and directories navigation
alias ls='ls -h --group-directories-first --time-style=locale --color=auto -F'
alias ll='ls -lh --group-directories-first --time-style=locale --color=auto -F'
alias la='ls -lhA --group-directories-first --time-style=locale --color=auto -F'
#alias ls='exa -lgh --group-directories-first --time-style=default --color=auto -F'
#alias la='exa -lgha --group-directories-first --time-style=default --color=auto -F'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

# Re-initialize bashrc and Xresources
alias s='source $HOME/.bashrc'
alias xrdb='xrdb -merge $HOME/.Xresources'

# Basic flags to useful commands
alias grep='grep -i --color=auto'
alias xprop='xprop | grep WM_CLASS'
alias more='less'
alias cp="cp -i"
alias rm='rm -i'
alias df="df -h"
alias free="free -h"
alias mkd='mkdir -pv'

# Basic vim stuff
alias v='vim'
alias e='vim'
alias sv='sudo vim'
alias cfr='vim $HOME/.config/ranger/rc.conf' #Edit ranger's config
alias cfv='vim $HOME/.vimrc' #Edit vim's config

# Some ranger stuff
alias r='ranger'
alias sr='sudo ranger'

# youtube-dl stuff
alias YT='youtube-dl'
alias YTA-wav='youtube-dl -x --audio-format wav' # Download only audio
alias YTA-flac='youtube-dl -x --audio-format flac' # Download only audio
alias YTA-mp3='youtube-dl -x --audio-format mp3' # Download only audio
alias YTA-ogg='youtube-dl -x --audio-format vorbis' # Download only audio
alias YTA-opus='youtube-dl -x --audio-format opus' # Download only audio
alias YTA-best='youtube-dl -x --audio-format best' # Download only audio

# Misc
alias x='sxiv -ft *'
alias mpv-nv='mpv --no-video' # MPV w/o video
alias ogg2mp3='parallel ffmpeg -i "{}" "{.}.mp3" ::: *.opus'
alias dmpv='devour.sh mpv'
alias zathura='devour.sh zathura'
alias wipehistory='history -c && history -w'
alias n='newsboat'

## Obvious typos
alias cd..='cd ..'
alias cd.='cd .'
