#!/bin/bash

# This is a script file that utilizes dmenu prompt in order to choose
# which browser you widh to run.

# Usually installed browsers:
# firefox
# tor-browser
# surf

# Paths declarations
HOSTS_LIST=$HOME/.config/list-ssh-hosts.txt

pgrep -x dmenu && exit

choises=$(cat $HOSTS_LIST | awk '{print $1}')
[[ "$choises" = "" ]] && exit 1


chosen=$(echo "$choises" | dmenu -i -p "SSH to where?" | awk '{print $1}')
[[ "$chosen" = "" ]] && exit 1

ssh $chosen
