#!/bin/bash

# awesome icon declarations
icon_keyboard='\uf11c'
icon_hdd='\uf0a0'
icon_home='\uf015'
icon_volume='\uf028'
icon_mem='\uf2db'
icon_net='\uf0e8'
icon_temp='\uf2c9'
icon_bat_dis='\uf241'
icon_bat_cha='\uf0e7'
icon_bat_recy='\uf1b8'
icon_bat_plug='\uf1e6'
icon_calendar='\uf073'
icon_tray_separator='\uf0d9'

pgrep -x batterystatus.sh && kill


#printf $icon_bat_dis && echo $(cat /sys/class/power_supply/BAT0/capacity)%
for battery in /sys/class/power_supply/BAT?
do
	# Get its remaining capacity and charge status.
	capacity=$(cat "$battery"/capacity 2>/dev/null) || break
	status=$(sed "s/[Dd]ischarging/$(printf $icon_bat_dis" ")/;s/[Nn]ot charging//;s/[Cc]harging/$(printf $icon_bat_cha" ")/;s/[Uu]nknown/$(printf $icon_bat_recy" ")/;s/[Ff]ull/$(printf $icon_bat_plug" ")/" "$battery"/status)
	# If it is discharging and 25% or less, we will add a ❗ as a warning.
	[ "$capacity" -le 25 ] && [ "$status" = "🔋" ] && warn="❗"

	printf "%s%s%s%% " "$status" "$warn" " $capacity"
	unset warn
done | sed 's/ *$//'

