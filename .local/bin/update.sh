#!/bin/bash

# apt-updater
function apt-updater {
	sudo apt-get update &&
        sudo apt-get dist-upgrade -Vy &&
        sudo apt-get autoremove -y &&
        sudo apt-get autoclean &&
        sudo apt-get clean
}

if [[ -d /etc/pacman.d ]]
	then
		sudo pacman --noconfirm -Syu
	elif [[ -d /etc/apt ]]
	then
		apt-updater	
fi
