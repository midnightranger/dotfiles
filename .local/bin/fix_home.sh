#!/bin/bash
# Load up your directories...
Directories[0]="/home/yannis"

# Setup find correctly.
export IFS=$'\n'

# Loop through our array.
for x in ${Directories[@]}
    do 
        # Find all directories & subdirectories
        for i in $(find $x -type d) 
            do 
                # Fix Permissions
                chmod -c 770 $i
                chown -c yannis:yannis $i
            done

        # Find all Files
        for i in $(find $x -type f) 
            do 
                # Fix Permissions
                chmod -c 660 $i
                chown -c yannis:yannis $i
            done
    done
